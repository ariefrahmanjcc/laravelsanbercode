<?php
require 'animal.php';
require 'Frog.php';
require 'Ape.php';

$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "Name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br>";
echo "<br>";
echo "Name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->cold_blooded <br>";
echo "Jump : ";
$kodok->jump();
echo "<br>";
echo "Name : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold blooded : $sungokong->cold_blooded <br>";
echo "Yell : ";
$sungokong->yell();

?>