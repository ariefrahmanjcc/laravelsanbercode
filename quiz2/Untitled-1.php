<?php
function ubah_huruf($string){
    $abjad='abcdefghijklmnopqrstuvwxyz';
    $result='';
    for($i=0;$i<strlen($string);$i++){
        for($j=0;$j<strlen($abjad);$j++){
            if($string[$i]==$abjad[$j]){
                $result.=$abjad[strpos($abjad,$string[$i])+1];
            }
        }
    }
    return $result.'<br>';
}

function tukar_besar_kecil($string){
    $result='';
    for($i=0;$i<strlen($string);$i++){
        if(ctype_lower($string[$i])){
            $result.=strtoupper($string[$i]);
        } else $result.=strtolower($string[$i]);
    }
    return $result."<br>";
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>