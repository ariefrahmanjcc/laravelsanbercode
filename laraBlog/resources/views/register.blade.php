<!DOCTYPE html>
<html>
  <head>
    <title>Register</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method='POST'>
      @csrf
      <p>First name:</p>
      <input type="text" name="fname" />
      <p>Last name:</p>
      <input type="text" name="lname" />
      <p>Gender:</p>
      <input type="radio" value="male" name="gender" />
      <label for="male">Male</label><br />
      <input type="radio" value="female" name="gender" />
      <label for="female">Female</label><br />
      <input type="radio" value="other" name="gender" />
      <label for="female">Other</label><br />
      <p>Nationality:</p>
      <select name="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
      </select>
      <p>Language Spoken:</p>
      <input type="checkbox" name="lang1" value="bahasa"/>
      <label for="bahasa">Bahasa Indonesia</label><br />
      <input type="checkbox" name="lang2" value="english"/>
      <label for="english">English</label><br />
      <input type="checkbox" name="lang3" value="other"/>
      <label for="other">Other</label><br />
      <p>Bio:</p>
      <textarea name="bio" rows="10" cols="30"></textarea><br />
      <input type="submit" value="SignUp" />
    </form>
  </body>
</html>
