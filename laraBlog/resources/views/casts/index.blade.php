@extends('adminlte.master')

@section('title')
    Cast List
@endsection

@section('content')
@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
<a href="/casts/create" class="btn btn-primary mb-2">Add New Cast</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col" >Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->name}}</td>
                <td>{{$value->age}}</td>
                <td>{{$value->bio}}</td>
                <td class="btn-group">
                    <a href="/casts/{{$value->id}}" class="btn btn-info mr-2 rounded">Show</a>
                    <a href="/casts/{{$value->id}}/edit" class="btn btn-primary mr-2 rounded">Edit</a>
                    <form action="casts/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete" >
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection