@extends('adminlte.master')

@section('title')
    {{$cast->name}}'s Details
@endsection

@section('content')
<a href="/casts" class="btn btn-primary mb-2">Back</a>
<table class="table">
    <tbody>
        <tr>
            <td>Name</th>
            <td>{{$cast->name}}</td>
        </tr>
        <tr>
            <td>Age</th>
            <td>{{$cast->age}}</td>
        </tr>
        <tr>
            <td>Bio</th>
            <td>{{$cast->bio}}</td>
        </tr>             
    </tbody>
</table>
@endsection