@extends('adminlte.master')

@section('title')
    Edit {{$cast->name}}'s Details
@endsection

@section('content')
<form action="/casts/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="card-body">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{$cast->name}}">
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" class="form-control" id="age" placeholder="Enter age" name="age" value="{{$cast->age}}">
        @error('age')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" rows="3" id="bio" placeholder="Enter Bio" name="bio">{{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
        <button type="submit" class="btn btn-danger">Cancel</button>
  </div>
</form>
@endsection