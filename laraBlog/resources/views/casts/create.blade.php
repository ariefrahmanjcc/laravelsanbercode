@extends('adminlte.master')

@section('title')
    Add Cast
@endsection

@section('content')
    <form action="/casts" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{old('name','')}}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="number" class="form-control" id="age" placeholder="Enter age" name="age" value="{{old('age','')}}">
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" rows="3" id="bio" placeholder="Enter Bio" name="bio">{{old('bio','')}}</textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add</button>
      </div>
    </form>
@endsection